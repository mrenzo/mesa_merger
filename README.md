Mathieu Renzo, [mrenzo@flatironinstitute.org](mailto:mrenzo@flatironinstitute.org)

Code for the models of [Renzo, Cantiello, et al. 2020](https://ui.adsabs.harvard.edu/abs/2020ApJ...904L..13R/abstract).

# Models for the "Stellar merger scenario"

This repository contains the code to compute the evolution of
simple post-merger products. *N.B:* it does not model the dynamical
phase of the merger, but constructs a guess for the post-merger
stellar structure. 

We model the progenitors of black holes (BH) populating the PISN mass
gap. These stars are the result of the dynamical merger of two stars
below the PPISN regime forming a star with a small core and oversized
envelope.  Di Carlo [2019](https://ui.adsabs.harvard.edu/abs/2019MNRAS.487.2947D/abstract),
2020[a](https://ui.adsabs.harvard.edu/abs/2020MNRAS.498..495D/abstract),[b](https://ui.adsabs.harvard.edu/abs/2020MNRAS.497.1043D/abstract)
invoke mergers of 58+42Msun at Z=0.002-0.0002.
    
# Structure of the repository

`Z_0.0002_58_42/MESA_mergers/` contains the template MESA work
directories, also provided as a tarball (uploaded on
[zenodo/record/4062493](https://zenodo.org/record/4062493)). The
zenodo tarball is exactly what was used for the paper, the content in
the repository might have slightly evolved post-publication.

`Z_0.0002_58_42/RESULTS/` contains the output of our MESA models used
in the manuscript. As above, those are also on
[zenodo/record/4062493](https://zenodo.org/record/4062493).

`notebooks/` contains the ipython notebooks used to construct and
analyze the MESA models.

`draft` contains the manuscript of [Renzo, Cantiello et
al. 2020](https://ui.adsabs.harvard.edu/abs/2020ApJ...904L..13R/abstract).


# How to construct merger structures

1. Use `Z_0.0002_58_42/MESA_mergers/primary_MS` and
   `Z_0.0002_58_42/MESA_mergers/secondary_to_primary_TAMS` to run the
   two stars you want to merge. You need to hardcode in the latter the
   stopping time equal to the TAMS time of the donor.
2. Then use `Z_0.0002_58_42/MESA_mergers/merger/1_relax_mass` to create the merger model. This also
   needs hardcoding in the inlist the new value of the mass. It starts
   from the donor's final model.
3. Then use the notebook `Z_0.0002_58_42/MESA_mergers/init_merger.ipynb` to create the composition
   profile. In this notebook there are multiple options, not all
   working probably. This will create a file which will be used as
   input for `Z_0.0002_58_42/MESA_mergers/merger/2_relax_composition`
4. Use the final model of `Z_0.0002_58_42/MESA_mergers/merger/1_relax_mass` as starting point for
   `Z_0.0002_58_42/MESA_mergers/merger/2_relax_composition` and the file created in step 3 to
   initialize the composition.
5. Use the final model of `Z_0.0002_58_42/MESA_mergers/merger/relax_composition` as the starting
   point for `Z_0.0002_58_42/MESA_mergers/post-merger`. Setting the initial timestep to something
   small might prevent the run from starting.
