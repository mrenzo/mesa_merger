! ***********************************************************************
!
!   Copyright (C) 2010-2019  Bill Paxton & The MESA Team
!
!   this file is part of mesa.
!
!   mesa is free software; you can redistribute it and/or modify
!   it under the terms of the gnu general library public license as published
!   by the free software foundation; either version 2 of the license, or
!   (at your option) any later version.
!
!   mesa is distributed in the hope that it will be useful,
!   but without any warranty; without even the implied warranty of
!   merchantability or fitness for a particular purpose.  see the
!   gnu library general public license for more details.
!
!   you should have received a copy of the gnu library general public license
!   along with this software; if not, write to the free software
!   foundation, inc., 59 temple place, suite 330, boston, ma 02111-1307 usa
!
! ***********************************************************************

      module run_star_extras

      use star_lib
      use star_def
      use const_def
      use math_lib

      implicit none

      ! these routines are called by the standard run_star check_model
      contains

      subroutine extras_controls(id, ierr)
         integer, intent(in) :: id
         integer, intent(out) :: ierr
         type (star_info), pointer :: s
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return
         s% extras_startup => extras_startup
         s% extras_start_step => extras_start_step
         s% extras_check_model => extras_check_model
         s% extras_finish_step => extras_finish_step
         s% extras_after_evolve => extras_after_evolve
         s% how_many_extra_history_columns => how_many_extra_history_columns
         s% data_for_extra_history_columns => data_for_extra_history_columns
         s% how_many_extra_profile_columns => how_many_extra_profile_columns
         s% data_for_extra_profile_columns => data_for_extra_profile_columns

         s% how_many_extra_history_header_items => how_many_extra_history_header_items
         s% data_for_extra_history_header_items => data_for_extra_history_header_items
         s% how_many_extra_profile_header_items => how_many_extra_profile_header_items
         s% data_for_extra_profile_header_items => data_for_extra_profile_header_items
         s% other_wind=>Dutch_tanh_intrp_wind
       end subroutine extras_controls

       subroutine  Dutch_tanh_intrp_wind(id, Lsurf, Msurf, Rsurf, Tsurf, X, Y, Z, w, ierr)
         use chem_def
         use star_def
         integer, intent(in) :: id
         real(dp), intent(in) :: Lsurf, Msurf, Rsurf, Tsurf, X, Y, Z ! surface values (cgs)
         ! NOTE: surface is outermost cell. not necessarily at photosphere.
         ! NOTE: don't assume that vars are set at this point.
         ! so if you want values other than those given as args,
         ! you should use values from s% xh(:,:) and s% xa(:,:) only.
         ! rather than things like s% Teff or s% lnT(:) which have not been set yet.
         real(dp), intent(out) :: w ! wind in units of Msun/year (value is >= 0)
         integer, intent(out) :: ierr
         type (star_info), pointer :: s
         real(dp) ::  L1, M1, R1, T1, xsurf, eta, Zsolar, Zfe, &
              log10w, w1, w2, T_high, T_low, alfa, fe, &
              T_WR_full_on, T_WR_full_off
         w = 0
         ierr = 0
         call get_star_ptr(id,s,ierr)
         L1 = Lsurf
         M1 = Msurf
         R1 = Rsurf
         T1 = Tsurf
         Zsolar = 0.019d0
         !I use the same factor everywhere
         eta = s% Dutch_scaling_factor

         xsurf = X
         Zfe = s% Zbase !! we don't want to use the actual Z polluted of CNO, but the baseline (see Tramper et al. 16)
         T_high = 11000d0
         T_low = 10000d0
         T_WR_full_off = 31622.8d0 !log(T_WR_full_off)=4.5
         T_WR_full_on = 50118.7d0  !log(T_WR_full_on)=4.7

         if ((xsurf < 0.4d0) .and. (T1 > T_WR_full_on)) then
            ! WR wind: surface H is low and T_surf > T_WR_full_on
            if(s%x_integer_ctrl(1)==1) then
               ! print *, 'using: N&L, eta=', eta
               w = 1d-11 * pow(L1/Lsun,1.29d0) * pow(Y,1.7d0) * sqrt(s% Zbase)
               w = w * eta
            else if(s% x_integer_ctrl(1) ==2) then
               ! print *, "using: tramper 2016", eta
               if(s%net_iso(ife56)==0) then
                  fe = s% Zbase/zsolar ! When we are using basic.net we dont have fe in the net
               else
                  fe = s% xa(s%net_iso(ife56),1)/(1.1686000000000001D-003)
               end if
               w = 10.d0**(-9.2d0) * pow(L1/Lsun,0.85d0) * pow(Y,0.44d0) * &
                    pow(fe,1.3d0) * eta
            end if
         else if ((xsurf <0.4d0) .and. ((T1>T_WR_full_off) .and. (T1< T_WR_full_on))) then
            ! interpolate Vink and WR wind for H-poor and T_WR_full_off <T_surf<T_WR_full_on
            ! print *, "interpolating Vink and WR wind"
            if(s%x_integer_ctrl(1)==1) then
               ! print *, 'using: N&L, eta=', eta
               w1 = 1d-11 * pow(L1/Lsun,1.29d0) * pow(Y,1.7d0) * sqrt(s% Zbase)
            else if(s% x_integer_ctrl(1) ==2) then
               ! print *, "using: tramper 2016", eta
               if(s%net_iso(ife56)==0) then
                  fe = s% Zbase/zsolar ! When we are using basic.net we dont have fe in the net
               else
                  fe = s% xa(s%net_iso(ife56),1)/(1.1686000000000001D-003)
               end if
               w1 = 10.d0**(-9.2d0) * pow(L1/Lsun,0.85d0) * pow(Y,0.44d0) * &
                    pow(fe,1.3d0)
            end if
            call eval_tanh_intrp_vink_wind(w2)
            alfa = (T1 - T_WR_full_off)/(T_WR_full_on - T_WR_full_off)
            w = (1-alfa)*w1 + alfa*w2
            w = w * eta
         else
            ! H-rich, use pure vink
            if (T1 <= T_low) then
               ! print *, 'using: de_Jager, eta=', eta
               call eval_de_Jager_wind(w)
               w = w * eta
            else if (T1 >= T_high) then
               ! print *, 'using: Vink_tanh, eta=', eta
               call eval_tanh_intrp_vink_wind(w)
               w = w * eta
            else               ! transition
               ! print *, 'interpolating de Jager and Vink_tanh, eta=', eta
               call eval_de_Jager_wind(w1)
               call eval_tanh_intrp_vink_wind(w2)
               alfa = (T1 - T_low)/(T_high - T_low)
               w = (1-alfa)*w1 + alfa*w2
               w = w * eta
            end if
         end if
         s% xtra(1) = w
       contains

         subroutine eval_de_Jager_wind(w)
           ! de Jager, C., Nieuwenhuijzen, H., & van der Hucht, K. A. 1988, A&AS, 72, 259.
           real(dp), intent(out) :: w
           real(dp) :: log10w
           include 'formats'
           log10w = 1.769d0*log10(L1/Lsun) - 1.676d0*log10(T1) - 8.158d0
           w = exp10(log10w)
         end subroutine eval_de_Jager_wind


         subroutine eval_tanh_intrp_vink_wind(w)
           real(dp), intent(inout) :: w
           real(dp) :: alfa, w1, w2, w_c, w_h, Teff_jump, logMdot, dT, vinf_div_vesc
           w = 0

           if (T1 > 27500d0) then
              vinf_div_vesc = 2.6d0 ! this is the hot side galactic value
              vinf_div_vesc = vinf_div_vesc*pow(Zfe/Zsolar,0.13d0) ! corrected for Z
              logMdot = &
                   - 6.697d0 &
                   + 2.194d0*log10(L1/Lsun/1d5) &
                   - 1.313d0*log10(M1/Msun/30) &
                   - 1.226d0*log10(vinf_div_vesc/2d0) &
                   + 0.933d0*log10(T1/4d4) &
                   - 10.92d0*pow2(log10(T1/4d4)) &
                   + 0.85d0*log10(Zfe/Zsolar)
              w1 = exp10(logMdot)
              w = w1
           else if (T1 < 22500d0) then
              vinf_div_vesc = 1.3d0 ! this is the cool side galactic value
              vinf_div_vesc = vinf_div_vesc*pow(Zfe/Zsolar,0.13d0) ! corrected for Z
              logMdot = &
                   - 6.688d0 &
                   + 2.210d0*log10(L1/Lsun/1d5) &
                   - 1.339d0*log10(M1/Msun/30) &
                   - 1.601d0*log10(vinf_div_vesc/2d0) &
                   + 1.07d0*log10(T1/2d4) &
                   + 0.85d0*log10(Zfe/Zsolar)
              w2 = exp10(logMdot)
              w = w2
           else ! use Vink et al 2001, eqns 14 and 15 to set "jump" temperature
              Teff_jump = 1d3*(61.2d0 + 2.59d0*(-13.636d0 + 0.889d0*log10(Zfe/Zsolar)))
              dT = 100d0
              ! print *, 'Teff_jump[K]', Teff_jump
              !     first evaluate the mass loss rate one would have if T = 27500, all rest held constant
              vinf_div_vesc = 2.6d0 ! this is the hot side galactic value
              vinf_div_vesc = vinf_div_vesc*pow(Zfe/Zsolar,0.13d0) ! corrected for Z
              w_h =  &
                   - 6.697d0 &
                   + 2.194d0*log10(L1/Lsun/1d5) &
                   - 1.313d0*log10(M1/Msun/30) &
                   - 1.226d0*log10(vinf_div_vesc/2d0) &
                   + 0.933d0*log10(27500/4d4) &
                   - 10.92d0*pow2(log10(27500/4d4)) &
                   + 0.85d0*log10(Zfe/Zsolar)
              w_h = exp10(w_h)
              !then evaluate the mass loss rate one would have if T= 22500, all rest held constant
              vinf_div_vesc = 1.3d0 ! this is the cool side galactic value
              vinf_div_vesc = vinf_div_vesc*pow(Zfe/Zsolar,0.13d0) ! corrected for Z
              w_c = &
                   - 6.688d0 &
                   + 2.210d0*log10(L1/Lsun/1d5) &
                   - 1.339d0*log10(M1/Msun/30) &
                   - 1.601d0*log10(vinf_div_vesc/2d0) &
                   + 1.07d0*log10(22500/2d4) &
                   + 0.85d0*log10(Zfe/Zsolar)
              w_c = exp10(w_c)
              !then interpolate between this two values
              w = ((w_h - w_c)/2)*tanh((T1-Teff_jump)/(1d0*dT)) &
                   +(w_c+w_h)/2
           end if
         end subroutine eval_tanh_intrp_vink_wind


       end subroutine  Dutch_tanh_intrp_wind


       subroutine extras_startup(id, restart, ierr)
         use chem_def
         integer, intent(in) :: id
         logical, intent(in) :: restart
         integer, intent(out) :: ierr
         character (len=200) :: fname
         real(dp), pointer ::  xa(:,:), xq(:), entropy(:)
         integer :: num_pts, species
         type (star_info), pointer :: s
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return
       end subroutine extras_startup


       integer function extras_start_step(id)
         integer, intent(in) :: id
         integer :: ierr
         type (star_info), pointer :: s
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return
         extras_start_step = 0
         s% xtra(1) = 0.0d0 ! to log wind mass loss

       end function extras_start_step


       ! returns either keep_going, retry, backup, or terminate.
       integer function extras_check_model(id)
         integer, intent(in) :: id
         integer :: ierr
         type (star_info), pointer :: s
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return
         extras_check_model = keep_going
         ! print *, "check star step:", "dotM", s% star_mdot
         if (extras_check_model == terminate) s% termination_code = t_extras_check_model
       end function extras_check_model


       integer function how_many_extra_history_columns(id)
         integer, intent(in) :: id
         integer :: ierr
         type (star_info), pointer :: s
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return
         how_many_extra_history_columns = 0
       end function how_many_extra_history_columns


       subroutine data_for_extra_history_columns(id, n, names, vals, ierr)
         integer, intent(in) :: id, n
         character (len=maxlen_history_column_name) :: names(n)
         real(dp) :: vals(n)
         integer, intent(out) :: ierr
         integer :: k
         type (star_info), pointer :: s
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return

       end subroutine data_for_extra_history_columns


       integer function how_many_extra_profile_columns(id)
         integer, intent(in) :: id
         integer :: ierr
         type (star_info), pointer :: s
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return
         how_many_extra_profile_columns = 0
       end function how_many_extra_profile_columns


       subroutine data_for_extra_profile_columns(id, n, nz, names, vals, ierr)
         integer, intent(in) :: id, n, nz
         character (len=maxlen_profile_column_name) :: names(n)
         real(dp) :: vals(nz,n)
         integer, intent(out) :: ierr
         type (star_info), pointer :: s
         integer :: k
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return
       end subroutine data_for_extra_profile_columns


       integer function how_many_extra_history_header_items(id)
         integer, intent(in) :: id
         integer :: ierr
         type (star_info), pointer :: s
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return
         how_many_extra_history_header_items = 0
       end function how_many_extra_history_header_items


       subroutine data_for_extra_history_header_items(id, n, names, vals, ierr)
         integer, intent(in) :: id, n
         character (len=maxlen_history_column_name) :: names(n)
         real(dp) :: vals(n)
         type(star_info), pointer :: s
         integer, intent(out) :: ierr
         ierr = 0
         call star_ptr(id,s,ierr)
         if(ierr/=0) return
       end subroutine data_for_extra_history_header_items


       integer function how_many_extra_profile_header_items(id)
         integer, intent(in) :: id
         integer :: ierr
         type (star_info), pointer :: s
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return
         how_many_extra_profile_header_items = 0
       end function how_many_extra_profile_header_items


       subroutine data_for_extra_profile_header_items(id, n, names, vals, ierr)
         integer, intent(in) :: id, n
         character (len=maxlen_profile_column_name) :: names(n)
         real(dp) :: vals(n)
         type(star_info), pointer :: s
         integer, intent(out) :: ierr
         ierr = 0
         call star_ptr(id,s,ierr)
         if(ierr/=0) return
       end subroutine data_for_extra_profile_header_items


       ! returns either keep_going or terminate.
       ! note: cannot request retry or backup; extras_check_model can do that.
       integer function extras_finish_step(id)
         integer, intent(in) :: id
         integer :: k, ierr
         type (star_info), pointer :: s
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return
         ! print *, "finish star step:", "dotM", s% star_mdot
         extras_finish_step = keep_going
         if (extras_finish_step == terminate) s% termination_code = t_extras_finish_step

       end function extras_finish_step


       subroutine extras_after_evolve(id, ierr)
         integer, intent(in) :: id
         integer, intent(out) :: ierr
         type (star_info), pointer :: s
         ierr = 0
         call star_ptr(id, s, ierr)
         if (ierr /= 0) return
       end subroutine extras_after_evolve


     end module run_star_extras
